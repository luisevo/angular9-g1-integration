import { Injectable } from '@angular/core';
import { GlxLangModule } from './lang.module';
import { TranslateService } from '@ngx-translate/core';
import { dayJs } from '../../utils';

@Injectable({
  providedIn: GlxLangModule
})
export class GlxLangService {

  constructor(private traslate: TranslateService) {}

  use(lang: string) {
    dayJs.locale(lang);
    this.traslate.use(lang);
  }

}
